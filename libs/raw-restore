#!/bin/sh
retvallocalmenu=0
localsplit="Off"	
localzip="GZip"
while [ "$retvallocalmenu" != "1" ];do
    $DIALOG --clear --backtitle "$backtitle" \
        --title "Restore Mode" \
        --menu "Choose one of the following options: \n\n" 0 0 0 \
        "A: Pick drive"  "Select target/source drive ($localdrive)" \
        "B: Config filename"  "Set filename ($localimagename)" \
        "C: Restore"  "Restore drive/partition from local drive" \
        2> $tempfilelocalmenu

    retvallocalmenu=$?
    choicelocalmenu=`cat $tempfilelocalmenu`
    case $retvallocalmenu in
        $DIALOG_OK)
            case $choicelocalmenu in
                "A: Pick drive")
                    $DIALOG --backtitle "$backtitle" \
                        --title "PICK DRIVE" \
                        --menu "Choose drive to write to/read from.\
                        \nSupported FS are:\n\
                        Reiserfs, ext2/3/4 - Linux\n\
                        fat32/ntfs - Windows95/98/NT/2k/XP\n\
                        \n\nSelect partition:" 0 0 0 \
                        $partlist 2> $tempfilelocaldrive

                    retvallocaldrive=$?
                    choicelocaldrive=`cat $tempfilelocaldrive`

                    case $retvallocaldrive in
                        $DIALOG_OK)
                            localdrive=$choicelocaldrive
                            mount_device /dev/$localdrive /mnt/local
                            if [ $? != 0 ] ; then
                                localdrive=""
                            fi
                            ;;

                        $DIALOG_CANCEL)
                            ;;

                        $DIALOG_ESC)
                            ;;
                    esac
                    ;;

                "B: Config filename")
                    $DIALOG --backtitle "$backtitle" \
                        --title "CONFIG FILENAME" --clear \
                        --fselect /mnt/local/ 7 60 2> $tempfilelocalname

                    retvallocalname=$?

                    case $retvallocalname in
                        $DIALOG_OK)
                            localimagename=`cat $tempfilelocalname`
                            ;;

                        $DIALOG_CANCEL)
                            ;;

                        $DIALOG_ESC)
                            if test -s $tempfilelocalname ; then
                                localimagename=`cat $tempfilelocalname`
                            fi
                            ;;
                    esac
                    ;;

                "C: Restore")
                    if [ "$localdrive" = "" ] ; then
                        $DIALOG --backtitle "$backtitle" \
                            --title "ERROR" --clear \
                            --msgbox "No target drive set! \n
                        ->Back to Main Menu." 0 0
                        continue
                    fi

                    if [ "$localimagename" = "" ] ; then
                        $DIALOG --backtitle "$backtitle" \
                            --title "WARNING" --clear \
                            --msgbox "No filename for image set, using default \n
                        Filename: image.img.gz" 0 0
                        localimagename=image.img.gz
                    fi

                    $DIALOG --backtitle "$backtitle" \
                        --title "RESTORE" \
                        --menu "Choose Drive or Partition to restore\n\
                        Select from:" 0 0 0 \
                        $binary_partlist 2> $tempfilelocalrest

                    retvallocalrest=$?
                    choicelocalrest=`cat $tempfilelocalrest`
                    case $retvallocalrest in
                        $DIALOG_OK)
                            localrest=/dev/$choicelocalrest
                            $DIALOG --backtitle "$backtitle" \
                                --title "ABOUT TO RESTORE" --clear \
                                --yesno "Collected information: \
                                \nTarget drive in /mnt/local: $localdrive \
                                \nFilename for image: $localimagename \
                                \nSplitting: $localsplit \
                                \nCompression: $localzip \
                                \nTarget drive: $localrest \
                                \n\nAre you sure?" 0 0

                            case $? in
                                $DIALOG_OK)
                                    localwritesize=`cat /proc/partitions |grep $choicelocalrest | head -n 1 |awk '{print $3}' `
                                    pcnt=0
                                    msg="Locally restoring your drive.\n"
                                    touch /tmp/progress.out
                                    if [ "$localzip" = "None" ] ; then
                                        if [ "$localsplit" = "Off" ] ; then
                                            (dd if=/mnt/local/$localimagename 2>/dev/null |jetcat-mod -p $localwritesize 2>/tmp/progress.out |dd of=$localrest 2>/dev/null) &
                                            progress
                                        fi

                                        if [ "$localsplit" = "On" ] ; then
                                            (cat /mnt/local/$localimagename.* |jetcat-mod -p $localwritesize 2>/tmp/progress.out |dd of=$localrest 2>/dev/null) &
                                            progress
                                        fi
                                    fi

                                    if [ "$localzip" = "GZip" ] ; then
                                        if [ "$localsplit" = "Off" ] ; then
                                            (dd if=/mnt/local/$localimagename 2>/dev/null |gunzip -c - |jetcat-mod -p $localwritesize 2>/tmp/progress.out |dd of=$localrest 2>/dev/null) &
                                            progress
                                        fi
                                        if [ "$localsplit" = "On" ] ; then
                                            (cat /mnt/local/$localimagename.* |gunzip -c - |jetcat-mod -p $localwritesize 2>/tmp/progress.out |dd of=$localrest 2>/dev/null) &
                                            progress
                                        fi
                                    fi

                                    if [ "$localzip" = "Lzop" ] ; then
                                        if [ "$localsplit" = "Off" ] ; then
                                            (dd if=/mnt/local/$localimagename 2>/dev/null |lzop -d -c - |jetcat-mod -p $localwritesize 2>/tmp/progress.out |dd of=$localrest 2>/dev/null) &
                                            progress
                                        fi
                                        if [ "$localsplit" = "On" ] ; then
                                            (cat /mnt/local/$localimagename.* |lzop -d  -c - |jetcat-mod -p $localwritesize 2>/tmp/progress.out |dd of=$localrest 2>/dev/null) &
                                        fi
                                    fi

                                    if [ "$localzip" = "BZip2" ] ; then
                                        if [ "$localsplit" = "Off" ] ; then
                                            (dd if=/mnt/local/$localimagename 2>/dev/null |bzcat |jetcat-mod -p $localwritesize 2>/tmp/progress.out |dd of=$localrest 2>/dev/null) &
                                            progress
                                        fi

                                        if [ "$localsplit" = "On" ] ; then
                                            (bzcat /mnt/local/$localimagename.* |jetcat-mod -p $localwritesize 2>/tmp/progress.out | dd of=$localrest 2>/dev/null) &
                                            progress
                                        fi
                                    fi

                                    umount /mnt/local
                                    rm /tmp/out
                                    touch /tmp/out
                                    ;;

                                $DIALOG_CANCEL)
                                    ;;

                                $DIALOG_ESC)
                                    ;;
                            esac
                            ;;
                        $DIALOG_CANCEL)
                            ;;
                        $DIALOG_ESC)
                            ;;
                    esac
                    ;;
            esac
            ;;
    esac
done
