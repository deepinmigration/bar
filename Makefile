all: jetcat-mod squashfs-tools

squashfs-tools:
	cd thirdparty/squashfs-tools && make 
	touch squashfs-tools

install:
	mkdir -p $(DESTDIR)/lib/bar
	find libs -type f -exec cp {} $(DESTDIR)/lib/bar \;
	install -Dm755 bar $(DESTDIR)/bin/bar
	install -Dm755 jetcat-mod $(DESTDIR)/bin/jetcat-mod
	rm -rf $(DESTDIR)/usr/share/bar
	mkdir -p $(DESTDIR)/usr/share/bar
	cp -r initramfs-tools $(DESTDIR)/usr/share/bar
	install -Dm755 15_linux_bar $(DESTDIR)/etc/grub.d/15_linux_bar
	find $(DESTDIR)/usr/share/bar/initramfs-tools/scripts -exec chmod a+x {} \;
	install -Dm755 thirdparty/squashfs-tools/mksquashfs $(DESTDIR)/lib/bar/mksqfs
	install -Dm755 thirdparty/squashfs-tools/unsquashfs $(DESTDIR)/lib/bar/unsqfs
	find $(DESTDIR)/lib/bar/ -type f -exec chmod a+x {} \;

clean:
	rm -f squashfs-tools
	cd thirdparty/squashfs-tools && make clean
	rm -f jetcat-mod
