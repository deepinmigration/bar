# BAR is short for Backup and Restore
**Ghost** for Deepin.

**bar** is a backup and restore tools for linux, which is Ghost like to save disk or partition to a image file.

# Features
* Raw compressed images of Disks or Partitions
* Squashfs compressed images of Partition contents

## Generate initramfs
Use `/lib/bar/generate-initramfs` will generate initramfs.img for the lastest kernel version
or you should generate initramfs manully by `/usr/sbin/mkinitramfs -d /usr/share/bar/initramfs-tools -o /boot/bar.img ${KERVER_VERSION}`

# Roadmap

## DONE
* dd backend
* squashfs backend
* grub.cfg support
* autostart with initramfs
* disable plymouth start and udev quit

## TODO
* usb disk lost partition type when pluggined
* restore by squashfs should sync uuid or fstype changes to /etc/fstype and /boot/grub/grub.cfg
* disable tty1 stdout and stderr output
